#!/usr/bin/env bash

set -e
set -x

CI_PAGES_DOMAIN=${1}
CI_PAGES_URL=${2}
CI_PROJECT_TITLE=${3}
CI_PROJECT_URL=${4}
COMMIT_TIME=${5}
GITLAB_USER_NAME=${6}
GITLAB_USER_EMAIL=${7}
CI_COMMIT_SHA=${8}
CI_PROJECT_VISIBILITY=${9}

script_dir=$(dirname "$0")
dist_dir=${script_dir}/../public
src_dir=${script_dir}/../src

mkdir -p $dist_dir
rsync -aH $src_dir/ $dist_dir

function archive_n_pages() {
  pushd ${dist_dir}
  find -L . -type f -name "*.npages" | sort | xargs -d "\n" tar -cvzf npages.tar.gz
  popd
}

function n_pages() {
  if [ -z "$1" ]
  then
    >&2 echo "Require one (1) argument <input-pdf>" >&2
  else
    pdfinfo "${1}" | awk '/^Pages:/ {print $2}' | tr -d '[:space:]' > "${1}.npages"
  fi
}

function archive_file_size() {
  pushd ${dist_dir}
  find -L . -type f -name "*.file-size" | sort | xargs -d "\n" tar -cvzf file-size.tar.gz
  popd
}

function file_size() {
  if [ -z "$1" ]
  then
    >&2 echo "Require one (1) argument <input-pdf>" >&2
  else
    pdfinfo "${1}" | awk '/^File size:/ {print $3}' | tr -d '[:space:]' > "${1}.file-size"
  fi
}

function metadata() {
  if [ -z "$1" ]
  then
    >&2 echo "Require one (1) argument <input-pdf>" >&2
  else
    n_pages "${1}"
    file_size "${1}"
  fi
}

function split_pdf() {
  if [ -z "$3" ]
  then
    >&2 echo "Require three (3) arguments <input-pdf> <pages> <output-pdf>" >&2
  else
    pdftk "${1}" cat "${2}" output "${dist_dir}/${3}"
    metadata "${dist_dir}/${3}"
  fi
}

per_source_file() {
  pre_src_files=$(find -L ${src_dir} -type f | sort)

  for pre_src_file in ${pre_src_files}; do
    pre_src_file_relative=$(realpath --relative-to=${src_dir} ${pre_src_file})
    pre_src_file_relative_dirname=$(dirname ${pre_src_file_relative})
    pre_src_file_basename=$(basename -- "${pre_src_file_relative}")
    pre_src_file_extension="${pre_src_file_relative##*.}"

    dist_dir_relative=${dist_dir}/${pre_src_file_relative_dirname}
    mkdir -p ${dist_dir_relative}

    if [ ${pre_src_file_extension} = "fodt" ] || \
      [ ${pre_src_file_extension} = "fodg" ] || \
      [ ${pre_src_file_extension} = "fods" ] || \
      [ ${pre_src_file_extension} = "txt"  ] || \
      [ ${pre_src_file_extension} = "html" ]; then
      sed -e "s#\${CI_PAGES_DOMAIN}#${CI_PAGES_DOMAIN}#g" \
          -e "s#\${CI_PAGES_URL}#${CI_PAGES_URL}#g" \
          -e "s#\${CI_PROJECT_TITLE}#${CI_PROJECT_TITLE}#g" \
          -e "s#\${CI_PROJECT_URL}#${CI_PROJECT_URL}#g" \
          -e "s#\${COMMIT_TIME}#${COMMIT_TIME}#g" \
          -e "s#\${GITLAB_USER_NAME}#${GITLAB_USER_NAME}#g" \
          -e "s#\${GITLAB_USER_EMAIL}#${GITLAB_USER_EMAIL}#g" \
          -e "s#\${CI_COMMIT_SHA}#${CI_COMMIT_SHA}#g" \
          -e "s#\${CI_PROJECT_VISIBILITY}#${CI_PROJECT_VISIBILITY}#g" \
          ${pre_src_file} > ${dist_dir_relative}/${pre_src_file_basename}
    else if [ ${pre_src_file_extension} = "md" ]; then
      (echo -e "# ${CI_PROJECT_TITLE}" && \
      echo -e "\n----\n" && \
      cat ${pre_src_file} && \
      echo -e "\n----\n" && \
      echo -e "### This document\n\n" && \
      echo -e "* hosted at **[${CI_PAGES_URL}](${CI_PAGES_URL})**\n" && \
      echo -e "* source hosted at **[${CI_PROJECT_URL}](${CI_PROJECT_URL})**\n" && \
      echo -e "* last updated at time **${COMMIT_TIME}**\n" && \
      echo -e "* last updated by user **[${GITLAB_USER_NAME}](mailto:${GITLAB_USER_EMAIL})**\n" && \
      echo -e "* revision **[${CI_COMMIT_SHA}](${CI_PROJECT_URL}/-/commit/${CI_COMMIT_SHA})**\n" && \
      echo -e "* access control **${CI_PROJECT_VISIBILITY}**\n" && \
      echo -e "\n----\n" && \
      cat ${share_dir}/licence.md &&\
      echo -e "\n----\n"
      ) > ${dist_dir_relative}/${pre_src_file_basename}
    else
      rsync -aH ${pre_src_file} ${dist_dir_relative}/${pre_src_file_basename}
    fi
    fi
  done
}

# RAAus Operations Manual 7.1
function raaus_operations_manual_7_1() {
  raaus_ops_manual71_path="5-om-71-august-2016-single-pages"
  raaus_ops_manual71=${dist_dir}/${raaus_ops_manual71_path}
  raaus_ops_manual71_pdf=${raaus_ops_manual71}.pdf

  if [ -f ${raaus_ops_manual71_pdf} ]; then
    metadata ${raaus_ops_manual71_pdf}
    split_pdf ${raaus_ops_manual71_pdf} 5-11 ${raaus_ops_manual71_path}_table-of-contents.pdf
    split_pdf ${raaus_ops_manual71_pdf} 34-39 ${raaus_ops_manual71_path}_2.01-pilot-certificate-group-a-and-b.pdf
    split_pdf ${raaus_ops_manual71_pdf} 47-49 ${raaus_ops_manual71_path}_2.04-pilot-certificates-aeroplane-groups.pdf
    split_pdf ${raaus_ops_manual71_pdf} 50-51 ${raaus_ops_manual71_path}_2.06-student-or-converting-pilot-certificate.pdf
    split_pdf ${raaus_ops_manual71_pdf} 52-61 ${raaus_ops_manual71_path}_2.07-pilot-certificate-group-a-and-b.pdf
  fi
}

# RAAus Operations Manual 7.1.1 (31 March 2021)
function raaus_operations_manual_7_1_1() {
  raaus_ops_manual711_path="raaus-operations-manual-issue-711"
  raaus_ops_manual711=${dist_dir}/${raaus_ops_manual711_path}
  raaus_ops_manual711_pdf=${raaus_ops_manual711}.pdf

  if [ -f ${raaus_ops_manual711_pdf} ]; then
    metadata ${raaus_ops_manual711_pdf}
    split_pdf ${raaus_ops_manual711_pdf} 4-5 ${raaus_ops_manual711_path}_table-of-contents.pdf
    split_pdf ${raaus_ops_manual711_pdf} 23-27 ${raaus_ops_manual711_path}_2.01-pilot-certificate-group-a-and-b.pdf
    split_pdf ${raaus_ops_manual711_pdf} 33 ${raaus_ops_manual711_path}_2.04-pilot-certificates-aeroplane-groups.pdf
    split_pdf ${raaus_ops_manual711_pdf} 34 ${raaus_ops_manual711_path}_2.04-pilot-qualifications.pdf
    split_pdf ${raaus_ops_manual711_pdf} 36-37 ${raaus_ops_manual711_path}_2.06-student-or-converting-pilot-certificate.pdf
    split_pdf ${raaus_ops_manual711_pdf} 38-45 ${raaus_ops_manual711_path}_2.07-pilot-certificate-group-a-and-b.pdf
  fi
}

# RAAus Operations Manual 7.1.2 (17 July 2024)
function raaus_operations_manual_7_1_2() {
  raaus_ops_manual712_path="RAAus-Flight-Operations-Manual-issue712"
  raaus_ops_manual712=${dist_dir}/${raaus_ops_manual712_path}
  raaus_ops_manual712_pdf=${raaus_ops_manual712}.pdf

  if [ -f ${raaus_ops_manual712_pdf} ]; then
    metadata ${raaus_ops_manual712_pdf}
    split_pdf ${raaus_ops_manual712_pdf} 4-5 ${raaus_ops_manual712_path}_table-of-contents.pdf
    split_pdf ${raaus_ops_manual712_pdf} 23-27 ${raaus_ops_manual712_path}_2.01-pilot-certificate-group-a-and-b.pdf
    split_pdf ${raaus_ops_manual712_pdf} 33 ${raaus_ops_manual712_path}_2.04-pilot-certificates-aeroplane-groups.pdf
    split_pdf ${raaus_ops_manual712_pdf} 34 ${raaus_ops_manual712_path}_2.04-pilot-qualifications.pdf
    split_pdf ${raaus_ops_manual712_pdf} 36-37 ${raaus_ops_manual712_path}_2.06-student-or-converting-pilot-certificate.pdf
    split_pdf ${raaus_ops_manual712_pdf} 38-45 ${raaus_ops_manual712_path}_2.07-pilot-certificate-group-a-and-b.pdf
    split_pdf ${raaus_ops_manual712_pdf} 51-53 ${raaus_ops_manual712_path}_2.08-instructor-rating-group-a-and-b.pdf
    split_pdf ${raaus_ops_manual712_pdf} 57-61 ${raaus_ops_manual712_path}_2.09-senior-instructor-rating-group-a-and-b.pdf
    split_pdf ${raaus_ops_manual712_pdf} 65-68 ${raaus_ops_manual712_path}_2.10-cfi-approval-group-a-and-b.pdf
  fi
}

# RAAus Syllabus of Flight Training 7
function raaus_syllabus_flight_training_7() {
  raaus_syllabus_flight_training7_path="1-syllabus-of-flight-training-issue-7-v2-single-pages-1"
  raaus_syllabus_flight_training7=${dist_dir}/${raaus_syllabus_flight_training7_path}
  raaus_syllabus_flight_training7_pdf=${raaus_syllabus_flight_training7}.pdf

  if [ -f ${raaus_syllabus_flight_training7_pdf} ]; then
    metadata ${raaus_syllabus_flight_training7_pdf}
    split_pdf ${raaus_syllabus_flight_training7_pdf} 6-19 ${raaus_syllabus_flight_training7_path}_table-of-contents.pdf
    split_pdf ${raaus_syllabus_flight_training7_pdf} 21-29 ${raaus_syllabus_flight_training7_path}_1.01-group-a-3-axis-syllabus.pdf
    split_pdf ${raaus_syllabus_flight_training7_pdf} 54 ${raaus_syllabus_flight_training7_path}_1.04-passenger-carrying-endorsement-syllabus.pdf
    split_pdf ${raaus_syllabus_flight_training7_pdf} 55 ${raaus_syllabus_flight_training7_path}_1.05-cross-country-endorsement-syllabus.pdf
    split_pdf ${raaus_syllabus_flight_training7_pdf} 56-58 ${raaus_syllabus_flight_training7_path}_1.06-formation-endorsement-syllabus.pdf
    split_pdf ${raaus_syllabus_flight_training7_pdf} 67-70 ${raaus_syllabus_flight_training7_path}_1.08-low-level-endorsement-syllabus.pdf
    split_pdf ${raaus_syllabus_flight_training7_pdf} 101-117 ${raaus_syllabus_flight_training7_path}_2.01-basic-aeronautical-knowledge-syllabus.pdf
    split_pdf ${raaus_syllabus_flight_training7_pdf} 118-120 ${raaus_syllabus_flight_training7_path}_2.02-air-legislation-syllabus.pdf
    split_pdf ${raaus_syllabus_flight_training7_pdf} 121-127 ${raaus_syllabus_flight_training7_path}_2.03-navigation-and-meteorology-syllabus.pdf
    split_pdf ${raaus_syllabus_flight_training7_pdf} 128-129 ${raaus_syllabus_flight_training7_path}_2.04-radio-operator-syllabus.pdf
    split_pdf ${raaus_syllabus_flight_training7_pdf} 130-132 ${raaus_syllabus_flight_training7_path}_2.05-human-factors-syllabus.pdf
  fi
}

# RAAus Technical Manual 4
function raaus_technical_manual_4() {
  raaus_technical_manual4_path="2-raaus-technical-manual-issue-4-single-pages"
  raaus_technical_manual4=${dist_dir}/${raaus_technical_manual4_path}
  raaus_technical_manual4_pdf=${raaus_technical_manual4}.pdf

  if [ -f ${raaus_technical_manual4_pdf} ]; then
    metadata ${raaus_technical_manual4_pdf}
    split_pdf ${raaus_technical_manual4_pdf} 9-11 ${raaus_technical_manual4_path}_table-of-contents.pdf
    split_pdf ${raaus_technical_manual4_pdf} 13-16 ${raaus_technical_manual4_path}_abbreviations-and-definitions.pdf
    split_pdf ${raaus_technical_manual4_pdf} 43-51 ${raaus_technical_manual4_path}_5.1-aircraft-registration.pdf
    split_pdf ${raaus_technical_manual4_pdf} 103-107 ${raaus_technical_manual4_path}_12.1-daily-and-pre-flight-inspections.pdf
  fi
}

# RAAus Technical Manual 4.1 (31 March 2021)
function raaus_technical_manual_4_1() {
  raaus_technical_manual41_path="raaus-technical-manual-issue-41"
  raaus_technical_manual41=${dist_dir}/${raaus_technical_manual41_path}
  raaus_technical_manual41_pdf=${raaus_technical_manual41}.pdf

  if [ -f ${raaus_technical_manual41_pdf} ]; then
    metadata ${raaus_technical_manual41_pdf}
    split_pdf ${raaus_technical_manual41_pdf} 7-8 ${raaus_technical_manual41_path}_table-of-contents.pdf
    split_pdf ${raaus_technical_manual41_pdf} 9-11 ${raaus_technical_manual41_path}_abbreviations-and-definitions.pdf
    split_pdf ${raaus_technical_manual41_pdf} 37-38 ${raaus_technical_manual41_path}_5.1-aircraft-registration.pdf
    split_pdf ${raaus_technical_manual41_pdf} 78-82 ${raaus_technical_manual41_path}_12.1-daily-and-pre-flight-inspections.pdf
  fi
}

# RAAus Technical Manual 4.3 (02 December 2024)
function raaus_technical_manual_4_3() {
  raaus_technical_manual43_path="raaus-technical-manual-issue-43"
  raaus_technical_manual43=${dist_dir}/${raaus_technical_manual43_path}
  raaus_technical_manual43_pdf=${raaus_technical_manual43}.pdf

  if [ -f ${raaus_technical_manual43_pdf} ]; then
    metadata ${raaus_technical_manual43_pdf}
    split_pdf ${raaus_technical_manual43_pdf} 3-4 ${raaus_technical_manual43_path}_table-of-contents.pdf
    split_pdf ${raaus_technical_manual43_pdf} 5-9 ${raaus_technical_manual43_path}_abbreviations-and-definitions.pdf
    split_pdf ${raaus_technical_manual43_pdf} 27-33 ${raaus_technical_manual43_path}_5.1-aircraft-registration.pdf
    split_pdf ${raaus_technical_manual43_pdf} 68 ${raaus_technical_manual43_path}_12.1-daily-and-pre-flight-inspections.pdf
    split_pdf ${raaus_technical_manual43_pdf} 88-89 ${raaus_technical_manual43_path}_12.7-pilot_maintenance.pdf
    split_pdf ${raaus_technical_manual43_pdf} 90 ${raaus_technical_manual43_path}_12.8-annual_and_periodic_maintenance.pdf
    split_pdf ${raaus_technical_manual43_pdf} 91-93 ${raaus_technical_manual43_path}_13.1-defect_reporting_and_airworthiness_notices.pdf
    split_pdf ${raaus_technical_manual43_pdf} 94 ${raaus_technical_manual43_path}_13.2-immediately_reportable_and_routine_reportable_matters.pdf
  fi
}

libreoffice_files() {
  libre_files=$(find -L ${dist_dir} -name '*.fodg' -o -name '*.fodt' -o -name '*.fods' -o -name '*.odg' -o -name '*.odt' -type f | sort)

  for libre_file in ${libre_files}; do
    libre_file_relative=$(realpath --relative-to=${dist_dir} ${libre_file})
    libre_file_relative_dirname=$(dirname ${libre_file_relative})
    libre_file_basename=$(basename -- "${libre_file_relative}")
    libre_file_extension="${libre_file_relative##*.}"
    libre_file_filename="${libre_file_basename%.*}"

    dist_dir_relative=${dist_dir}/${libre_file_relative_dirname}
    mkdir -p ${dist_dir_relative}

    for format in pdf html docx; do
      libreoffice --invisible --headless --convert-to ${format} ${libre_file} --outdir ${dist_dir_relative}
      metadata ${dist_dir_relative}/${libre_file_filename}.pdf
    done
  done
}

function make_page1() {
  pdf_files=$(cd ${dist_dir} && find . -type f -name '*.pdf')

  IFS=$'\n'
  for pdf_file in $pdf_files
  do
    basename=$(basename -- "${pdf_file}")
    dirname=$(dirname -- "${pdf_file}")
    filename="${basename%.*}"

    page1_pdf_dir=${dist_dir}/${dirname}
    page1_pdf="${page1_pdf_dir}/${filename}_page1.pdf"

    mkdir -p "${page1_pdf_dir}"

    gs -q -sDEVICE=pdfwrite -dNOPAUSE -dBATCH -dSAFER -dFirstPage=1 -dLastPage=1 -sOutputFile=${page1_pdf} ${dist_dir}/${pdf_file}

    page1_pdf_png="${page1_pdf}.png"

    convert ${page1_pdf} ${page1_pdf_png}

    for size in 600 450 350 250 150 100 65 45
    do
      page1_pdf_png_size="${page1_pdf}-${size}.png"
      convert ${page1_pdf_png} -resize ${size}x${size} ${page1_pdf_png_size}
    done
  done
  IFS="$OIFS"
}

per_source_file
raaus_operations_manual_7_1
raaus_operations_manual_7_1_1
raaus_operations_manual_7_1_2
raaus_syllabus_flight_training_7
raaus_technical_manual_4
raaus_technical_manual_4_1
raaus_technical_manual_4_3
libreoffice_files
make_page1
archive_n_pages
archive_file_size
